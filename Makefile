bin/matmul: src/matriz.c
	gcc -Wall -fsanitize=address,undefined -pthread src/matriz.c -o bin/matmul -g

.PHONY: clean
clean:
	rm bin/matmul
run:
	bin/matmul --filas_m1 2000 --cols_m1 800 --filas_m2 800 --cols_m2 3000 --n_hilos 8

gdb:
	gdb bin/matmul
	